class Foo {
    constructor(name) {
        this.name = name;

        this.greet = () => { // using function statement doesn't bind context.
            console.log(this.name);
        }

        // bind context of this instance to greet2 function
        this.greet2 = this.greet2.bind(this);
    }

    greet2() {
        console.log(this.name);
    }
}


const foo1 = new Foo("foo1");
const foo2 = new Foo("foo2");

const functionToGreet = foo1.greet2;

functionToGreet.call(foo2);
// foo2.greet();
