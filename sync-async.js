// sync
function doSync() {
    console.log("doSync executed");
}

// async with callback
function doAsync(callback) {
    setTimeout(function () {
        console.log("doAsync executed");
        callback("foo");
    }, 500);
}

function doAsyncWithPromise() {
    return new Promise(function (resolve/* , reject */) {
        setTimeout(function () {
            console.log("doAsync executed");
            resolve("foo");
        }, 500);
    });
}

async function doAsyncWithAsync() {
    console.log("async with async");
    const result = await doAsyncWithPromise();
    console.log("async after await with result " + result);
}



// doSync();

// doAsync(function (result) {
//     console.log("doAsync done with result: " + result);
//     doAsync(function () {
//         console.log("done die zweite");
//     });
// });

doAsyncWithPromise()
  .then(function (result) {
    return doAsyncWithPromise();
  })
  .then(function () {

  });

doAsyncWithAsync();

// doSync();
