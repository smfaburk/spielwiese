const EventEmitter = require("events");

class Foo {
    constructor() {
        this.name = "myname";

        this.greet = this.greet.bind(this);
    }

    greet() {
        console.log(this);
        console.log("hello " + this.name);
    }
}

const ee = new EventEmitter();
const foo = new Foo();

ee.on("hello", function () {
    console.log("hello called");
});
ee.on("hello", foo.greet);

ee.emit("hello");
