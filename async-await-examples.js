// Beispiel mit Promises
function sync(exitOnError) {
    return Promise.resolve()
        .then(() => {
            // create plant tree if no tree is available
            if (!plantTree) {
                // build plant tree
                plantTree = new PlantTree();
                return plantTree.buildTree(configuration.plantsToProcess);
            }
            return null;
        })
        .then(() => {
            return plantTree.syncData();
        })
        .catch((error) => {
            logger.error('Error occured while syncing data:');
            logger.error(error.stack);
            if (exitOnError) {
                process.exit(-1);
            }
        });
}

// Beispiel mit async/await
async function sync(exitOnError) {
    try {
        // create plant tree if no tree is available
        if (!plantTree) {
            // build plant tree
            plantTree = new PlantTree();
            await plantTree.buildTree(configuration.plantsToProcess);
        }
        await plantTree.syncData();
    } catch (error) {
        logger.error('Error occured while syncing data:');
        logger.error(error.stack);
        if (exitOnError) {
            process.exit(-1);
        }
    }
}